#![deny(clippy::all, unsafe_code)]

use clap::{App, Arg};
use subprocess::{Exec, Redirection};

fn main() {
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .version(env!("CARGO_PKG_VERSION"))
        .arg(Arg::with_name("keyword").index(1).possible_values(&[
            "Ciphers",
            "KexAlgorithms",
            "MACs",
        ]))
        .get_matches();

    let keyword = match (matches.is_present("keyword"), matches.value_of("keyword")) {
        (true, Some(k)) => k,
        _ => {
            eprintln!("{}", matches.usage());
            std::process::exit(1);
        }
    };

    match keyword {
        "Ciphers" => {
            let setting = PreferenceList {
                is_deny: &is_weak_cipher,
                list_support: get_list_support(&ssh_version()),
                keyword: "Ciphers",
                values: supported_ssh_ciphers(),
            };
            println!("{}", setting);
        }
        "KexAlgorithms" => {
            let setting = PreferenceList {
                is_deny: &is_weak_kex,
                list_support: get_list_support(&ssh_version()),
                keyword: "KexAlgorithms",
                values: supported_ssh_kexs(),
            };
            println!("{}", setting);
        }
        "MACs" => {
            let setting = PreferenceList {
                is_deny: &is_weak_mac,
                list_support: get_list_support(&ssh_version()),
                keyword: "MACs",
                values: supported_ssh_macs(),
            };
            println!("{}", setting);
        }
        _ => {
            eprintln!("{}", matches.usage());
            std::process::exit(1);
        }
    }
}

#[derive(Debug, PartialEq)]
enum ListSupport {
    AllowList,
    DenyList,
}

struct PreferenceList<'a> {
    is_deny: &'a dyn Fn(&str) -> bool,
    list_support: ListSupport,
    keyword: &'a str,
    values: Vec<String>,
}
impl<'a> std::fmt::Display for PreferenceList<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let values: Vec<String> = self
            .values
            .iter()
            .filter_map(|value| match (&self.list_support, (self.is_deny)(value)) {
                (ListSupport::AllowList, false) => Some(value.clone()),
                (ListSupport::DenyList, true) => Some(format!("-{}", value.clone())),
                _ => None,
            })
            .collect();
        if values.is_empty() {
            return write!(f, "");
        }
        write!(f, "{} {}", self.keyword, values.join(","))
    }
}

fn get_list_support(ssh_version: &str) -> ListSupport {
    let re = regex::Regex::new(r"OpenSSH_(\d+\.\d+)").unwrap();
    for caps in re.captures_iter(ssh_version) {
        let version = caps.get(1).unwrap().as_str();
        if let Ok(v) = version.parse::<f32>() {
            // OpenSSH 7.5 supports deny lists, 7.4 and older doesn't
            return if v >= 7.5 {
                ListSupport::DenyList
            } else {
                ListSupport::AllowList
            };
        }
    }
    // default
    ListSupport::DenyList
}

fn is_weak_cipher(cipher: &str) -> bool {
    let cbc_re = regex::Regex::new(r"\bcbc\b").unwrap();
    let rc4_re = regex::Regex::new(r"\b(arcfour|rc4)").unwrap();
    // no ending word-boundary: need to catch "arcfour" and "arcfour128"

    // CBC ciphers are weak: https://access.redhat.com/solutions/420283
    // RC4 ciphers are weak: https://en.wikipedia.org/wiki/RC4#Security
    cbc_re.is_match(cipher) || rc4_re.is_match(cipher)
}

fn is_weak_kex(kex: &str) -> bool {
    let sha1_re = regex::Regex::new(r"\bsha1\b").unwrap();

    // SHA1 is weak: https://en.wikipedia.org/wiki/SHA-1
    sha1_re.is_match(kex)
}

fn is_weak_mac(mac: &str) -> bool {
    let bits96_re = regex::Regex::new(r"\b96\b").unwrap();
    let md5_re = regex::Regex::new(r"\bmd5\b").unwrap();
    let sha1_re = regex::Regex::new(r"\bsha1\b").unwrap();

    // 96-bit algorithms are weak: https://access.redhat.com/solutions/420283
    // MD5 is weak: https://access.redhat.com/solutions/420283
    // SHA1 is weak: https://en.wikipedia.org/wiki/SHA-1
    bits96_re.is_match(mac) || md5_re.is_match(mac) || sha1_re.is_match(mac)
}

fn ssh(args: &[&str]) -> subprocess::Result<String> {
    Ok(Exec::cmd("ssh")
        .args(args)
        .stdout(Redirection::Pipe)
        .stderr(Redirection::Merge)
        .capture()?
        .stdout_str())
}

fn ssh_version() -> String {
    match ssh(&["-V"]) {
        Ok(output) => output,
        Err(_) => String::from(""),
    }
}

fn supported_ssh_ciphers() -> Vec<String> {
    match ssh(&["-Q", "cipher"]) {
        Ok(output) => output.lines().map(|l| String::from(l.trim())).collect(),
        Err(_error) => Vec::<String>::new(),
    }
}

fn supported_ssh_kexs() -> Vec<String> {
    match ssh(&["-Q", "kex"]) {
        Ok(output) => output.lines().map(|l| String::from(l.trim())).collect(),
        Err(_error) => Vec::<String>::new(),
    }
}

fn supported_ssh_macs() -> Vec<String> {
    match ssh(&["-Q", "mac"]) {
        Ok(output) => output.lines().map(|l| String::from(l.trim())).collect(),
        Err(_error) => Vec::<String>::new(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_list_support_by_versions() {
        let old_version = "OpenSSH_7.4p1 Debian-10+deb9u3, OpenSSL 1.0.2l  25 May 2017";
        assert_eq!(get_list_support(old_version), ListSupport::AllowList);
        assert_eq!(get_list_support(""), ListSupport::DenyList);
    }
}
