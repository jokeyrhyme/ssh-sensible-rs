# ssh-sensible-rs

tooling to produce sensible settings for SSH

## usage example

```
$ ssh-sensible Ciphers
Ciphers -3des-cbc,-aes128-cbc,-aes192-cbc,-aes256-cbc

$ ssh-sensible Ciphers >> ~/.ssh/config
```


